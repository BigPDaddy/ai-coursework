﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FiniteCarManager : MonoBehaviour {

    // Public car object and it's corresponding script
    public GameObject car;
    CarScript carScript;

    // Public text objects
    public Text distText;
    public Text velYText;
    public Text stateText;

    // Public floats for distance limits
    public float nearLeft;
    public float nearMidL;
    public float nearMidR;
    public float nearRight;

    // Public floats for velocity limits
    public float nearLeftVel;
    public float nearMidLVel;
    public float nearMidRVel;
    public float nearRightVel;

    // Public racing line object and it's corresponding line renderer
    public GameObject racingLine;
    LineRenderer racingLineRend;

    // Public object for the test panel
    public GameObject testPanel;

    float velocity; // Velocity to store the car's rigidbody velocity
    float speed = 10.0f; // Speed variable for acceleration and decceleration
    float lineMove = 0.0f; // Line move value to set the line's Y position
    float distance, linePos, carPos; // Float variables for working out distance from car to the line

    // Create a state enumerator for the switch statement
    private enum State { Drive = 0, Stop = 1, HardLeft = 2, HardRight = 3, SoftLeft = 4, SoftRight = 5 };
    private State carState = State.Drive;

    // Bool to check if test panel is open
    bool mode = false;

    // Coroutine to reset the scene called after 3 seconds
    IEnumerator ResetScene()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("FiniteScene");
    }

    private void Start()
    {
        // Set the component objects at startup
        carScript = car.GetComponent<CarScript>();
        racingLineRend = racingLine.GetComponent<LineRenderer>();

        // Set the test panel to be inactive on startup
        testPanel.SetActive(false);
    }

    private void Update()
    {
        // Set the line and car positions each frame
        carPos = car.transform.position.y;
        linePos = racingLineRend.GetPosition(0).y;

        // Set the velocity value every frame
        velocity = car.GetComponent<Rigidbody2D>().velocity.y;

        // Calculate distance between the car and line
        distance = linePos - carPos;

        // Set test panel's active status based on pause bool
        if(mode)
        {
            testPanel.SetActive(true);
        }
        else
        {
            testPanel.SetActive(false);
        }

        // Only handle line input if paused
        if(mode == false)
        {
            // Handle input for line movement
            if (Input.GetKey(KeyCode.UpArrow))
            {
                lineMove += 0.2f;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                lineMove -= 0.2f;
            }
            if(Input.GetKey(KeyCode.Space))
            {
                lineMove = 0.0f;
            }
        
            // Clamp the line position to be within the track bounds
            lineMove = Mathf.Clamp(lineMove, -2.5f, 2.5f);
        }

        // Set the position of each of the line's vertices
        for (int p = 0; p < racingLineRend.positionCount; p++)
        {
            racingLineRend.SetPosition(p, new Vector3(p * 150, lineMove, 1.0f));
        }

        // Set the text values
        distText.text = "Distance: " + distance.ToString("0.00");
        velYText.text = "Velocity Y: " + velocity.ToString("0.0");
        stateText.text = "Current State: " + carState.ToString();
    }

    // Fixed update is called once per tick of the physics engine
    void FixedUpdate()
    {
        // Switch statement for the FSM
        switch(carState)
        {
            case State.Drive:
                MoveCar();
                break;
            case State.Stop:
                StopCar();
                break;
            case State.HardLeft:
                TurnHardLeft();
                break;
            case State.HardRight:
                TurnHardRight();
                break;
            case State.SoftLeft:
                TurnSoftLeft();
                break;
            case State.SoftRight:
                TurnSoftRight();
                break;
            default:
                break;
        }
    }

    // Move the car forward
    void MoveCar()
    {
        // If game isn't paused
        if (mode == false)
        {
            //Move the car forward
            carScript.Accelerate(speed, distance);
            Debug.Log("Driving Straight");
        }

        // Check for finish line
        if(carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }
        // Check for soft right
        if(((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }
        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }
    }

    // Stop the car
    void StopCar()
    {
        // Slow the car to a halt
        // Since the accelerate function isn't called, the car slows automatically
        Debug.Log("Stopped");
        StartCoroutine("ResetScene");
    }

    // Hard left turn when car is far right
    void TurnHardLeft()
    {
        // If game isn't paused
        if (mode == false)
        {
            carScript.Accelerate(speed, distance);
            carScript.TurnHard(distance);
            Debug.Log("Turning Hard Left");
        }

        // Check for finish
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if ((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }
        // Check for soft right
        if (((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }

        // Check for middle
        if((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }

    // Hard right turn when car is far left
    void TurnHardRight()
    {
        // If game isn't paused
        if (mode == false)
        {
            carScript.Accelerate(speed, distance);
            carScript.TurnHard(distance);
            Debug.Log("Turning Hard Right");
        }

        // Check for finish
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }
        
        // Check for soft right
        if (((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }
        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }


        // Check for middle
        if ((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }

    }

    // Soft left turn when the car is closer to the line
    void TurnSoftLeft()
    {
        // If game isn't paused
        if (mode == false)
        {
            carScript.Accelerate(speed, distance);
            carScript.TurnSoft(distance);
            Debug.Log("Turning Soft Left");
        }

        // Check for finish
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if ((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }
        // Check for soft right
        if (((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }

        // Check for middle
        if ((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }

    // Soft right turn when the car is closer to the line
    void TurnSoftRight()
    {
        // If game isn't paused
        if (mode == false)
        {
            carScript.Accelerate(speed, distance);
            carScript.TurnSoft(distance);
            Debug.Log("Turning Soft Right");
        }

        // Check for finish
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if ((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }
        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }

        // Check for middle
        if ((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }

    // Pause toggle function
    public void ToggleMode()
    {
        mode = !mode;
    }

    // Return to the main menu
    public void ReturnHome()
    {
        SceneManager.LoadScene("Menu");
    }
} 