﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Basic 2D Car Movement
// Parts of the code from Unity Tutorial: 2d Arcade-Style Car Driving/Physics by quill18creates on YouTube

public class CarScript : MonoBehaviour {

    Rigidbody2D rb; // Rigidbody of the car
    public GameObject finishLine; 

    public bool finished = false; // Bool for stopping the car
    public float speedForce = 10.0f; // Force for forward velocity
    public float torqueForce = -200.0f; // Torque force for acceleration
    float driftFactorSticky = 0.9f; // Drift factor for when not drifting heavily
    float driftFactorSlippy = 1f; // Drift factor for skidding
    float maxStickyVelocity = 2.5f; // Max velocity for sticky drifting
    Quaternion carRotation; // Quaternion to steer the car
    public float steering = 0.0f; // Steering value to be applied to the quaternion

    public void Start()
    {
        // Acquire the rigidbody2d and initial rotation of the car
        rb = GetComponent<Rigidbody2D>();
        carRotation = transform.rotation;
    }

    // Runs once per tick of the physics engine
    void FixedUpdate()
    {
        // Set default drift value to non-skidding
        float driftFactor = driftFactorSticky;

        // If car starts to skid set the drift factor to slippy
        if (SideVelocity().magnitude > maxStickyVelocity)
        {
            driftFactor = driftFactorSlippy;
        }

        // Simulate skidding
        rb.velocity = ForwardVelocity() + (SideVelocity() * driftFactor);

        transform.rotation = carRotation * Quaternion.Euler(0.0f, 0.0f, steering);
    }

    // Overloaded acceleration function for FSM
    public void Accelerate(float force, float distance)
    {
        // Accelerate the car at a constant speed
        rb.AddForce(transform.right * force);
        // Multiply the steering value by distance to line to get a smoother steer
        steering = distance;
    }

    // Overloaded acceleration function for FIS
    public void Accelerate(float force)
    {
        // Accelerate the car at a constant speed
        rb.AddForce(transform.right * force);
    }

    // Decceleration function
    public void Deccelerate(float force)
    {
        // Reverse the car function
        // Not used for anything currently
    }

    // Function for a hard turn
    public void TurnHard(float distance)
    {
        // Distance will be negative for a right turn making sure it will go the right way
        steering = 10 * distance;
    }

    // Function for a soft turn
    public void TurnSoft(float distance)
    {
        // Distance will be negative for a right turn making sure it will go the right way
        steering = 5 * distance;
    }

    // Return forward velocity vector of the car
    Vector2 ForwardVelocity()
    {
        return transform.right * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.right);
    }

    // Return sideways velocity vector of the car
    Vector2 SideVelocity()
    {
        return transform.up * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.up);
    }

    // Function to steer the car in the FIS scene
    public void FuzzySteering(double fuzzySteering)
    {
        steering = (float)fuzzySteering;
    }

    // When the collider exits the trigger (The enter function must be called first)
    private void OnTriggerExit2D(Collider2D collision)
    {
        // If the car crosses the finish line
      if(collision.GetComponent<Collider2D>().gameObject.tag == "Finish")
      {
          // finished being true causes the scene to reset in FSM and FIS examples
          Debug.Log("Finished!!");
          finished = true;
      }
    }
}
