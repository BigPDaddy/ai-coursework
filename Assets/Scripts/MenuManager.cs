﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    // Load FIS
    public void LoadFuzzy()
    {
        SceneManager.LoadScene("FuzzyScene");
    }

    // Load FSM
    public void LoadFinite()
    {
        SceneManager.LoadScene("FiniteScene");
    }
}
