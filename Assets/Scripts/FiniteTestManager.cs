﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FiniteTestManager : MonoBehaviour {

    // Public car object and script for the tast car image
    public GameObject car;
    CarScript carScript;

    // Public text objects
    public Text distText;
    public Text velYText;
    public Text stateText;
    public Text angularText;

    // Input text from input fields
    public Text inputDText;
    public Text inputVText;

    // Float values for parsed input text
    float inV, inD;

    // Public floats for distance limits
    public float nearLeft;
    public float nearMidL;
    public float nearMidR;
    public float nearRight;

    // Public floats for velocity limits
    public float nearLeftVel;
    public float nearMidLVel;
    public float nearMidRVel;
    public float nearRightVel;

    float velocity; // Car's Velocity
    float distance; // Distance from the Line

    // Create a state enumerator for the switch statement
    private enum State { Drive = 0, Stop = 1, HardLeft = 2, HardRight = 3, SoftLeft = 4, SoftRight = 5 };
    private State carState = State.Drive;

    private void Start()
    {
        // Set the component objects at startup
        carScript = car.GetComponent<CarScript>();
    }

    private void Update()
    {
        // Parse the input distance text to a float
        inD = float.Parse(inputDText.text);
        distance = inD;

        // Parse the input velocity text to a float
        inV = float.Parse(inputVText.text);
        velocity = inV;
        
        // Set the text values
        distText.text = "Distance: " + distance.ToString("0.00");
        velYText.text = "Velocity Y: " + velocity.ToString("0.0");
        stateText.text = "Current State: " + carState.ToString();
        angularText.text = "Steering Output: " + carScript.steering;
    }

    // Fixed update is called once per tick of the physics engine
    void FixedUpdate()
    {
        // Switch statement for the FSM
        switch(carState)
        {
            case State.Drive:
                MoveCar();
                break;
            case State.Stop:
                StopCar();
                break;
            case State.HardLeft:
                TurnHardLeft();
                break;
            case State.HardRight:
                TurnHardRight();
                break;
            case State.SoftLeft:
                TurnSoftLeft();
                break;
            case State.SoftRight:
                TurnSoftRight();
                break;
            default:
                break;
        }
    }

    // Move the car forward
    void MoveCar()
    {
        // Check for finish line
        if(carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }
        // Check for soft right
        if(((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }
        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }
    }

    // Stop the car
    void StopCar()
    {
        Debug.Log("Stopped");
    }

    // Hard left turn when car is far right
    void TurnHardLeft()
    {
        carScript.TurnHard(distance);
        Debug.Log("Turning Hard Left");

        // Check for stop
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if ((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }
        // Check for soft right
        if (((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }

        // Check for middle
        if((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }

    // Hard right turn when car is far left
    void TurnHardRight()
    {
        carScript.TurnHard(distance);
        Debug.Log("Turning Hard Right");

        // Check for stop
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }
        
        // Check for soft right
        if (((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }
        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }

        // Check for middle
        if ((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }

    // Soft left turn when the car is closer to the line
    void TurnSoftLeft()
    {
        carScript.TurnSoft(distance);
        Debug.Log("Turning Soft Left");

        // Check for stop
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if ((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }
        // Check for soft right
        if (((distance < nearMidR && distance >= nearRight) && velocity <= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity <= nearRightVel)))
        {
            carState = State.SoftRight;
        }

        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }

        // Check for middle
        if ((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }

    // Soft right turn when the car is closer to the line
    void TurnSoftRight()
    {
        carScript.TurnSoft(distance);
        Debug.Log("Turning Soft Right");

        // Check for stop
        if (carScript.finished == true)
        {
            carState = State.Stop;
        }

        // Check for hard right
        if ((distance < nearRight) || ((distance < nearMidR && distance >= nearRight) && velocity > 0))
        {
            carState = State.HardRight;
        }

        // Check for soft left
        if (((distance > nearMidL && distance <= nearLeft) && velocity >= 0) || ((distance <= nearMidL && distance >= nearMidR) && (velocity >= nearLeftVel)))
        {
            carState = State.SoftLeft;
        }
        // Check for hard left
        if ((distance > nearLeft) || ((distance > nearMidL && distance <= nearLeft) && velocity < 0))
        {
            carState = State.HardLeft;
        }

        // Check for middle
        if ((distance <= nearMidL && distance >= nearMidR) && (velocity < nearLeftVel && velocity > nearRightVel))
        {
            carState = State.Drive;
        }
    }
} 