﻿using System.Collections;
using System.Collections.Generic;
using FLS; // https://github.com/davidgrupp/Fuzzy-Logic-Sharp
using FLS.Rules;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class FuzzyCarManager : MonoBehaviour {

    // Car objects
    public GameObject car;
    Rigidbody2D rb;
    CarScript carScript;

    // Racing line objects
    public GameObject racingLine;
    LineRenderer racingLineRend;

    // Public text variables
    public Text velText;
    public Text distText;
    public Text fuzzyText;
    public Text velText2;
    public Text distText2;
    public Text fuzzyText2;

    // Text from the input fields
    public Text velInput;
    public Text distInput;

    // Test panel object and test car
    public GameObject testPanel;
    public Image testCar;

    // Floats for car speed and line & car positions
    float speed = 10.0f, linePos, carPos;
    // Line move value for changing line's y position
    float lineMove = 0.0f;

    // Double values for velocity and distance required for fuzzy engine
    // Converted back to float after
    double carVelocity, lineDistance;

    // Input values for velocity and distance from test panel
    float inV;
    float inD;

    // Manual or test mode
    // false = running, true = paused
    bool mode = false;

    // Coroutine restes scene after 3 seconds
    IEnumerator ResetScene()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("FuzzyScene");
    }

    // Use this for initialization
    private void Start ()
    {
        // Acquire the script and rigidbody2d from the car
        carScript = car.GetComponent<CarScript>();
        rb = car.GetComponent<Rigidbody2D>();

        // Get the renderer from the line
        racingLineRend = racingLine.GetComponent<LineRenderer>();

        // Set test panel to inactive on startup
        testPanel.SetActive(false);
    }

    private void Update()
    {
        // Set the line and car positions each frame
        carPos = car.transform.position.y;
        linePos = racingLineRend.GetPosition(0).y;

        // Set the velocity value every frame
        carVelocity = rb.velocity.y;

        // Calculate distance between the car and line
        lineDistance = linePos - carPos;

        // Handle input for line movement
        if (Input.GetKey(KeyCode.UpArrow))
        {
            lineMove += 0.2f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            lineMove -= 0.2f;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            lineMove = 0.0f;
        }

        // Clamp the line position to be within the track bounds
        lineMove = Mathf.Clamp(lineMove, -2.5f, 2.5f);

        // Set test panel active status based on paused bool
        if(mode)
        {
            testPanel.SetActive(true);
        }
        else
        {
            testPanel.SetActive(false);
        }

        // Set the position of each of the line's vertices
        for (int p = 0; p < racingLineRend.positionCount; p++)
        {
            racingLineRend.SetPosition(p, new Vector3(p * 150, lineMove, 1.0f));
        }

        // Setup the distance input for FIS
        var distance = new LinguisticVariable("Distance");
        var farLeft = distance.MembershipFunctions.AddTrapezoid("FarLeft", 1.5, 2.5, 7.5, 8.5);
        var nearLeft = distance.MembershipFunctions.AddTriangle("NearLeft", 0.0, 1.8, 3.6);
        var middle = distance.MembershipFunctions.AddTriangle("Middle", -1.0, 0.0, 1.0);
        var nearRight = distance.MembershipFunctions.AddTriangle("NearRight", -3.6, -1.8, 0.0);
        var farRight = distance.MembershipFunctions.AddTrapezoid("FarRight", -8.5, -7.5, -2.5, -1.5);

        // Setup the velocity input for FIS
        var velocity = new LinguisticVariable("Velocity");
        var farLeftV = velocity.MembershipFunctions.AddTrapezoid("FarLeftV", 0.6, 1.0, 3.0, 3.4);
        var nearLeftV = velocity.MembershipFunctions.AddTriangle("NearLeftV", 0.0, 0.5, 1.0);
        var middleV = velocity.MembershipFunctions.AddTriangle("MiddleV", -0.4, 0, 0.4);
        var nearRightV = velocity.MembershipFunctions.AddTriangle("NearRightV", -1.0, -0.5, 0);
        var farRightV = velocity.MembershipFunctions.AddTrapezoid("FarRightV", -3.4, -3.0, -1.0, -0.6);

        // Setup the steering output for FIS
        var steering = new LinguisticVariable("Steering");
        var farLeftS = steering.MembershipFunctions.AddTriangle("FarLeftS", 0.0, 20.0, 40.0);
        var nearLeftS = steering.MembershipFunctions.AddTriangle("NearLeftS", 1.0, 8.5, 15.0);
        var middleS = steering.MembershipFunctions.AddTriangle("MiddleS", -6.0, 0.0, 6.0);
        var nearRightS = steering.MembershipFunctions.AddTriangle("NearRightS", -15.0, -8.5, -1.0);
        var farRightS = steering.MembershipFunctions.AddTriangle("FarRightS", -40.0, -20.0, 0.0);

        // Default defuzzification is CoG
        IFuzzyEngine fuzzyEngine = new FuzzyEngineFactory().Default();

        // Distance is far right rules
        var rule21 = Rule.If(distance.Is(farRight).And(velocity.Is(farLeftV))).Then(steering.Is(farRightS));
        var rule22 = Rule.If(distance.Is(farRight).And(velocity.Is(nearLeftV))).Then(steering.Is(farRightS));
        var rule23 = Rule.If(distance.Is(farRight).And(velocity.Is(middleV))).Then(steering.Is(farRightS));
        var rule24 = Rule.If(distance.Is(farRight).And(velocity.Is(nearRightV))).Then(steering.Is(farRightS));
        var rule25 = Rule.If(distance.Is(farRight).And(velocity.Is(farRightV))).Then(steering.Is(farRightS));

        // Distance is near right rules
        var rule16 = Rule.If(distance.Is(nearRight).And(velocity.Is(farLeftV))).Then(steering.Is(farRightS));
        var rule17 = Rule.If(distance.Is(nearRight).And(velocity.Is(nearLeftV))).Then(steering.Is(farRightS));
        var rule18 = Rule.If(distance.Is(nearRight).And(velocity.Is(middleV))).Then(steering.Is(nearRightS));
        var rule19 = Rule.If(distance.Is(nearRight).And(velocity.Is(nearRightV))).Then(steering.Is(nearRightS));
        var rule20 = Rule.If(distance.Is(nearRight).And(velocity.Is(farRightV))).Then(steering.Is(nearRightS));

        // Distance is middle rules
        var rule11 = Rule.If(distance.Is(middle).And(velocity.Is(farLeftV))).Then(steering.Is(nearRightS));
        var rule12 = Rule.If(distance.Is(middle).And(velocity.Is(nearLeftV))).Then(steering.Is(middleS));
        var rule13 = Rule.If(distance.Is(middle).And(velocity.Is(middleV))).Then(steering.Is(middleS));
        var rule14 = Rule.If(distance.Is(middle).And(velocity.Is(nearRightV))).Then(steering.Is(middleS));
        var rule15 = Rule.If(distance.Is(middle).And(velocity.Is(farRightV))).Then(steering.Is(nearLeftS));

        // Distance is near left rules
        var rule6 = Rule.If(distance.Is(nearLeft).And(velocity.Is(farLeftV))).Then(steering.Is(nearLeftS));
        var rule7 = Rule.If(distance.Is(nearLeft).And(velocity.Is(nearLeftV))).Then(steering.Is(nearLeftS));
        var rule8 = Rule.If(distance.Is(nearLeft).And(velocity.Is(middleV))).Then(steering.Is(nearLeftS));
        var rule9 = Rule.If(distance.Is(nearLeft).And(velocity.Is(nearRightV))).Then(steering.Is(farLeftS));
        var rule10 = Rule.If(distance.Is(nearLeft).And(velocity.Is(farRightV))).Then(steering.Is(farLeftS));

        // Distance is far left rules
        var rule1 = Rule.If(distance.Is(farLeft).And(velocity.Is(farLeftV))).Then(steering.Is(farLeftS));
        var rule2 = Rule.If(distance.Is(farLeft).And(velocity.Is(nearLeftV))).Then(steering.Is(farLeftS));
        var rule3 = Rule.If(distance.Is(farLeft).And(velocity.Is(middleV))).Then(steering.Is(farLeftS));
        var rule4 = Rule.If(distance.Is(farLeft).And(velocity.Is(nearRightV))).Then(steering.Is(farLeftS));
        var rule5 = Rule.If(distance.Is(farLeft).And(velocity.Is(farRightV))).Then(steering.Is(farLeftS));

        // Add rules to fuzzy engine
        fuzzyEngine.Rules.Add(rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10, rule11, rule12, rule13, rule14, rule15, rule16, rule17, rule18, rule19, rule20, rule21, rule22, rule23, rule24, rule25);

        // Set a value to the defuzzified output
        double result = fuzzyEngine.Defuzzify(new { distance = lineDistance, velocity = carVelocity });
        
        // Test Engine - Current bug in library means that a new engine has to be made for every call to defuzzify
        var altdistance = new LinguisticVariable("AltDistance");
        var altfarLeft = altdistance.MembershipFunctions.AddTrapezoid("AltFarLeft", 1.5, 2.5, 7.5, 8.5);
        var altnearLeft = altdistance.MembershipFunctions.AddTriangle("AltNearLeft", 0.0, 1.8, 3.6);
        var altmiddle = altdistance.MembershipFunctions.AddTriangle("AltMiddle", -1.0, 0.0, 1.0);
        var altnearRight = altdistance.MembershipFunctions.AddTriangle("AltNearRight", -3.6, -1.8, 0.0);
        var altfarRight = altdistance.MembershipFunctions.AddTrapezoid("AltFarRight", -8.5, -7.5, -2.5, -1.5);

        var altvelocity = new LinguisticVariable("AltVelocity");
        var altfarLeftV = altvelocity.MembershipFunctions.AddTrapezoid("AltFarLeftV", 0.6, 1.0, 3.0, 3.4);
        var altnearLeftV = altvelocity.MembershipFunctions.AddTriangle("AltNearLeftV", 0.0, 0.5, 1.0);
        var altmiddleV = altvelocity.MembershipFunctions.AddTriangle("AltMiddleV", -0.4, 0, 0.4);
        var altnearRightV = altvelocity.MembershipFunctions.AddTriangle("AltNearRightV", -1.0, -0.5, 0);
        var altfarRightV = altvelocity.MembershipFunctions.AddTrapezoid("AltFarRightV", -3.4, -3.0, -1.0, -0.6);

        var altsteering = new LinguisticVariable("AltSteering");
        var altfarLeftS = altsteering.MembershipFunctions.AddTriangle("AltFarLeftS", 0.0, 20.0, 40.0);
        var altnearLeftS = altsteering.MembershipFunctions.AddTriangle("AltNearLeftS", 1.0, 8.5, 15.0);
        var altmiddleS = altsteering.MembershipFunctions.AddTriangle("AltMiddleS", -6.0, 0.0, 6.0);
        var altnearRightS = altsteering.MembershipFunctions.AddTriangle("AltNearRightS", -15.0, -8.5, -1.0);
        var altfarRightS = altsteering.MembershipFunctions.AddTriangle("AltFarRightS", -40.0, -20.0, 0.0);

        // Default defuzzification is CoG
        IFuzzyEngine altfuzzyEngine = new FuzzyEngineFactory().Default();

        // Distance is far right rules
        var altrule21 = Rule.If(altdistance.Is(altfarRight).And(altvelocity.Is(altfarLeftV))).Then(altsteering.Is(altfarRightS));
        var altrule22 = Rule.If(altdistance.Is(altfarRight).And(altvelocity.Is(altnearLeftV))).Then(altsteering.Is(altfarRightS));
        var altrule23 = Rule.If(altdistance.Is(altfarRight).And(altvelocity.Is(altmiddleV))).Then(altsteering.Is(altfarRightS));
        var altrule24 = Rule.If(altdistance.Is(altfarRight).And(altvelocity.Is(altnearRightV))).Then(altsteering.Is(altfarRightS));
        var altrule25 = Rule.If(altdistance.Is(altfarRight).And(altvelocity.Is(altfarRightV))).Then(altsteering.Is(altfarRightS));

        // Distance is near right rules
        var altrule16 = Rule.If(altdistance.Is(altnearRight).And(altvelocity.Is(altfarLeftV))).Then(altsteering.Is(altfarRightS));
        var altrule17 = Rule.If(altdistance.Is(altnearRight).And(altvelocity.Is(altnearLeftV))).Then(altsteering.Is(altfarRightS));
        var altrule18 = Rule.If(altdistance.Is(altnearRight).And(altvelocity.Is(altmiddleV))).Then(altsteering.Is(altnearRightS));
        var altrule19 = Rule.If(altdistance.Is(altnearRight).And(altvelocity.Is(altnearRightV))).Then(altsteering.Is(altnearRightS));
        var altrule20 = Rule.If(altdistance.Is(altnearRight).And(altvelocity.Is(altfarRightV))).Then(altsteering.Is(altnearRightS));

        // Distance is middle rules
        var altrule11 = Rule.If(altdistance.Is(altmiddle).And(altvelocity.Is(altfarLeftV))).Then(altsteering.Is(altnearRightS));
        var altrule12 = Rule.If(altdistance.Is(altmiddle).And(altvelocity.Is(altnearLeftV))).Then(altsteering.Is(altmiddleS));
        var altrule13 = Rule.If(altdistance.Is(altmiddle).And(altvelocity.Is(altmiddleV))).Then(altsteering.Is(altmiddleS));
        var altrule14 = Rule.If(altdistance.Is(altmiddle).And(altvelocity.Is(altnearRightV))).Then(altsteering.Is(altmiddleS));
        var altrule15 = Rule.If(altdistance.Is(altmiddle).And(altvelocity.Is(altfarRightV))).Then(altsteering.Is(altnearLeftS));

        // Distance is near left rules
        var altrule6 = Rule.If(altdistance.Is(altnearLeft).And(altvelocity.Is(altfarLeftV))).Then(altsteering.Is(altnearLeftS));
        var altrule7 = Rule.If(altdistance.Is(altnearLeft).And(altvelocity.Is(altnearLeftV))).Then(altsteering.Is(altnearLeftS));
        var altrule8 = Rule.If(altdistance.Is(altnearLeft).And(altvelocity.Is(altmiddleV))).Then(altsteering.Is(altnearLeftS));
        var altrule9 = Rule.If(altdistance.Is(altnearLeft).And(altvelocity.Is(altnearRightV))).Then(altsteering.Is(altfarLeftS));
        var altrule10 = Rule.If(altdistance.Is(altnearLeft).And(altvelocity.Is(altfarRightV))).Then(altsteering.Is(altfarLeftS));

        // Distance is far left rules
        var altrule1 = Rule.If(altdistance.Is(altfarLeft).And(altvelocity.Is(altfarLeftV))).Then(steering.Is(farLeftS));
        var altrule2 = Rule.If(altdistance.Is(altfarLeft).And(altvelocity.Is(altnearLeftV))).Then(steering.Is(farLeftS));
        var altrule3 = Rule.If(altdistance.Is(altfarLeft).And(altvelocity.Is(altmiddleV))).Then(steering.Is(farLeftS));
        var altrule4 = Rule.If(altdistance.Is(altfarLeft).And(altvelocity.Is(altnearRightV))).Then(steering.Is(farLeftS));
        var altrule5 = Rule.If(altdistance.Is(altfarLeft).And(altvelocity.Is(altfarRightV))).Then(steering.Is(farLeftS));

        // Add rules to alternate engine
        altfuzzyEngine.Rules.Add(altrule1, altrule2, altrule3, altrule4, altrule5, altrule6, altrule7, altrule8, altrule9, altrule10, altrule11, altrule12, altrule13, altrule14, altrule15, altrule16, altrule17, altrule18, altrule19, altrule20, altrule21, altrule22, altrule23, altrule24, altrule25);

        // Set test car value to alternate engines output using the test inputs as the input for the machine
        double test = altfuzzyEngine.Defuzzify(new { altdistance = (double)inD, altvelocity = (double)inV });

        // Turn the car and test car
        carScript.FuzzySteering(result);
        testCar.transform.rotation = transform.rotation * Quaternion.Euler(0.0f, 0.0f, (float)test);

        // Set text values
        velText.text = "Velocity: " + carVelocity.ToString("0.0");
        distText.text = "Distance: " + lineDistance.ToString("0.0");
        fuzzyText.text = "Fuzzy Output : " + result.ToString("0.00");
        fuzzyText2.text = "Fuzzy Output(-20 / 20): " + test.ToString("0.00");

        // Reset scene
        if (carScript.finished == true)
        {
            StartCoroutine("ResetScene");
        }
    }

    // Fixed update is called once per tick of the physics engine
    void FixedUpdate ()
    {
        // If game isn't paused
        if (mode == false)
        {
            // Drive the car
            if (carScript.finished == false)
            {
                carScript.Accelerate(speed);
            }
        }
	}

    // Toggle pause
    public void ToggleMode()
    {
        mode = !mode;
    }

    // Set the position of the line using the test input
    public void SetTestPosition()
    {
        inD = float.Parse(distInput.text);
        Debug.Log("input dist: " + inD);

        distText2.text = "Distance (-5 / 5): " + inD;
    }

    // Set the velocity of the line using the test input
    public void SetTestVelocity()
    {
        inV = float.Parse(velInput.text);
        Debug.Log("input vel: " + inV);

        velText2.text = "Velocity(-2 / 2): " + inV;
    }

    // Return to menu
    public void ReturnHome()
    {
        SceneManager.LoadScene("Menu");
    }
}
