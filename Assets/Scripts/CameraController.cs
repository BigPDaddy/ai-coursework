﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
	
	// Update is called once per frame
	void Update ()
    {
        // Mve the camera to the cars position every frame
        transform.position = new Vector3(target.position.x, target.position.y, -10.0f);
	}
}
